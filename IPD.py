import os
os.environ['ETS_TOOLKIT'] = 'qt4'
os.environ['QT_API'] = 'pyqt'
import sys

from pyface.qt import QtGui

from UserInterface import GUI

if __name__ == "__main__":
	app = QtGui.QApplication.instance()
	myapp = GUI.MainWindow()
	sys.exit(app.exec_())