# -*- coding: utf-8 -*-
"""
Created on Sun Nov 29 09:29:42 2015

@author: pierre
"""

import numpy as np
from scipy.interpolate import RectBivariateSpline
from Basis import Curve

class ProfilesDB():
	
	def __init__(self, rSampling, cSampling, DB = None):
		self.nR = rSampling
		self.nC = cSampling
		self.DB = {'0.2000': Profile.J3t(0.3, True, cSampling), '1.0000': Profile.Tulin(0.3, True, cSampling)}
# 		self.DB = {'0.2000': Profile(self.nC), '1.0000': Profile(self.nC)}
		if DB != None:
			self.DB = DB
		self.offsets = []
		self.thickness = []
	
	def get(self, r):
		if '{0:.4f}'.format(r) in self.DB:
			return self.DB['{0:.4f}'.format(r)]
		else:
			return None
	
	def set(self, r, profile):
		self.DB['{0:.4f}'.format(r)] = profile
	
	def add(self, r):
		radii = []
		for key in sorted(self.DB.iterkeys()):
			radii.append(float(key))
		rmin = min(radii)
		rmax = max(radii)
		if rmin == rmax:
			self.set(r, self.get(rmin).deepcopy())
		else:
			rdown = rmin
			rup = rmax
			for lr in radii:
				if lr < r  and lr > rdown:
					rdown = lr
				if lr > r and lr < rup:
					rup = lr
			profileInf = self.get(rdown)
			profileSup = self.get(rup)
			
			curveInf = profileInf.offsets
			curveSup = profileSup.offsets
			xs = set(curveInf.x + curveSup.x)
			xList = []
			yList = []
			for x in xs:
				xList.append(x)
				yList.append((curveInf.get(x)+curveSup.get(x))/2)
			offsets = Curve(self.nC, xValues=xList, yValues=yList)
			
			curveInf = profileInf.thickness
			curveSup = profileSup.thickness
			xs = set(curveInf.x + curveSup.x)
			xList = []
			yList = []
			for x in xs:
				xList.append(x)
				yList.append((curveInf.get(x)+curveSup.get(x))/2)
			thickness = Curve(self.nC, xValues=xList, yValues=yList)
			self.set(r, Profile(self.nC, offsetCurve=offsets, thicknessCurve=thickness))
	
	def getData(self, i, j):
		return self.offsets[i][j], self.thickness[i][j]
	
	def setSampling(self, nR, nC):
		self.nR = nR
		self.nC = nC
		
		for key in sorted(self.DB.iterkeys()):
			self.DB[key].setSampling(self.nC)
	
	def resample(self):
		xc = np.linspace(0, 1, self.nC)
		r = np.linspace(0.2, 1, self.nR)
		radii = []
		offsetsHD = []
		thicknessHD = []
		for key in sorted(self.DB.iterkeys()):
			profile = self.DB[key]
			radii.append(float(key))
			_, of = profile.offsets.resample()
			_, th = profile.thickness.resample()
			offsetsHD.append(of)
			thicknessHD.append(th)
		
		offsetsHD = np.array(offsetsHD)
		thicknessHD = np.array(thicknessHD)
		
		if len(radii) == 1:
			orderR = 0
		elif len(radii) == 2:
			orderR = 1
		elif len(radii) == 3:
			orderR = 2
		else:
			orderR = 3
		
		if len(xc) == 1:
			orderC = 0
		elif len(xc) == 2:
			orderC = 1
		elif len(xc) == 3:
			orderC = 2
		else:
			orderC = 3
		
		f_offsets = RectBivariateSpline(radii, xc, offsetsHD, kx=orderR, ky=orderC)
		self.offsets = f_offsets(r, xc)
		f_thickness = RectBivariateSpline(radii, xc, thicknessHD, kx=orderR, ky=orderC)
		self.thickness = f_thickness(r, xc)
	
	@staticmethod
	def getDBfromConfig(config, rSampling, cSampling):
		rList = config.get('Profiles_DataBase', 'RadiusValues').replace('{', '').replace('}', '').strip().split(',')
		DB = {}
		for radius in rList:
			DB[radius.strip()] = Profile.getProfilefromConfig(config, radius.strip(), cSampling)
		return ProfilesDB(rSampling, cSampling, DB)
	
	def save(self, config):
		rString = '{ '
		i = 0
		for key in sorted(self.DB.iterkeys()):
			prefix = ', '
			if i == 0:
				prefix = ''
			rString += prefix+key
			i += 1
		rString += ' }'
		config.add_section('Profiles_DataBase')
		config.set('Profiles_DataBase', 'RadiusValues', rString)
		
		for key in sorted(self.DB.iterkeys()):
			self.DB[key].save(config, key)

class Profile():
	
	def __init__(self, cSampling, offsetCurve=None, thicknessCurve=None):
		self.nC = cSampling
		self.offsets = Curve(cSampling, [0, 1], [0, 0])
		self.thickness = Curve(cSampling, [0, 1], [0, 0.05])
		if offsetCurve != None:
			self.offsets = offsetCurve
		if thicknessCurve != None:
			self.thickness = thicknessCurve
	
	def setSampling(self, nC):
		self.nC = nC
		self.offsets.sampling = self.nC
		self.thickness.sampling = self.nC
	
	@staticmethod
	def getProfilefromConfig(config, sectionName, sampling):
		offsets = Curve.getCurvefromConfig(config, sectionName+'_Offsets', sampling)
		thickness = Curve.getCurvefromConfig(config, sectionName+'_Thickness', sampling)
		return Profile(sampling, offsets, thickness)
	
	def save(self, config, sectionName):
		self.offsets.save(config, sectionName+'_Offsets')
		self.thickness.save(config, sectionName+'_Thickness')
	
	@staticmethod
	def Tulin(a1, correctAngle, sampling):
		x = np.linspace(0, 1, sampling)
		y = a1/2 * (x + 8.0/3.0*np.power(x, 1.5) - 4*np.power(x, 2))
		if correctAngle:
			theta = np.arctan(y[-1])
			xn = x*np.cos(theta) + y*np.sin(theta)
			yn = -x*np.sin(theta) + y*np.cos(theta)
			xmax = xn[-1]
			x = xn/xmax
			y = yn/xmax
		
		offsets = Curve(sampling, x, y)
		thickness = Curve(sampling, [0, 1], [0, 0.1])
		return Profile(sampling, offsetCurve=offsets, thicknessCurve=thickness)
	
	@staticmethod
	def J3t(a1, correctAngle, sampling):
		x = np.linspace(0, 1, sampling)
		y = a1/10 * (5*x - 20.0*np.power(x, 1.5) + 80*np.power(x, 2) - 64*np.power(x, 2.5))
		if correctAngle:
			theta = np.arctan(y[-1])
			xn = x*np.cos(theta) + y*np.sin(theta)
			yn = -x*np.sin(theta) + y*np.cos(theta)
			xmax = xn[-1]
			x = xn/xmax
			y = yn/xmax
		
		offsets = Curve(sampling, x, y)
		thickness = Curve(sampling, [0, 1], [0, 0.1])
		return Profile(sampling, offsetCurve=offsets, thicknessCurve=thickness)

	@staticmethod
	def getTulin(aoa, correctAngle, sampling):
		''
	
	@staticmethod
	def tulin(x):
		return (0.5 *(x + 8/3.0*np.power(x, 1.5) - 4*np.power(x, 2.0)))
	
	@staticmethod
	def tulinPrime(x):
		return (0.5 *(1 + 4.0*np.power(x, 0.5) - 8*x))
	
	@staticmethod
	def johnson3t(x):
		return (0.1 *(5*x - 20*np.power(x, 1.5) + 80*np.power(x, 2.0) - 64*np.power(x, 2.5)))
	
	@staticmethod
	def johnson3tPrime(x):
		return (0.1 *(5 - 30*np.power(x, 0.5) + 160*x - 160*np.power(x, 1.5)))