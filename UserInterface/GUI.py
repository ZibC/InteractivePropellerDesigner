import os
os.environ['ETS_TOOLKIT'] = 'qt4'
os.environ['QT_API'] = 'pyqt'
import sys

import matplotlib
matplotlib.use('Qt4Agg')
matplotlib.rcParams['backend.qt4']='PyQt4'

from pyface.qt import QtGui, QtCore

import PropellerModel as PM

from EditionFrames import GeneratorLineFrame, ProfileFrame, SettingsFrame, ShapeFrame

from VisuFrame import VTKFrame

class MainWindow(QtGui.QMainWindow):
	
	def __init__(self):
		super(MainWindow, self).__init__()
		
		self.propeller = None
		if len(sys.argv) >= 2:
			if os.path.exists(sys.argv[1]):
				print 'Read from file'
				self.propeller = PM.Model.Propeller.initializeFromFile(sys.argv[1])
				self.currentPath = sys.argv[1]
			else:
				print 'Invalid file path'
		
		if self.propeller is None:
			print 'Initialise from defaults'
			self.propeller = PM.Model.Propeller(0.04)
			self.currentPath = None
		
		self.setObjectName("MainWindow")
		self.setWindowTitle('Interactive Propeller Designer')
		self.resize(1280, 720)
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
		sizePolicy.setHorizontalStretch(0)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
		self.setSizePolicy(sizePolicy)
		
		self.centralwidget = QtGui.QWidget(self)
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
		sizePolicy.setHorizontalStretch(0)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.centralwidget.sizePolicy().hasHeightForWidth())
		self.centralwidget.setSizePolicy(sizePolicy)
		self.centralwidget.setAutoFillBackground(False)
		self.centralwidget.setObjectName("centralwidget")
		
		self.verticalLayout = QtGui.QVBoxLayout(self.centralwidget)
		self.verticalLayout.setSpacing(1)
		self.verticalLayout.setContentsMargins(1, 1, 1, 1)
		self.verticalLayout.setObjectName("verticalLayout")
		
		self.splitter = QtGui.QSplitter(self.centralwidget)
		self.splitter.setOrientation(QtCore.Qt.Horizontal)
		self.splitter.setObjectName("splitter")

		self.tabWidgetLeft = QtGui.QTabWidget(self.splitter)
		self.tabWidgetLeft.setObjectName("tabWidgetLeft")
		
		self.generatorLineFrame = GeneratorLineFrame(self.tabWidgetLeft, self.propeller)
		self.tabWidgetLeft.addTab(self.generatorLineFrame, "Generator line")
		
		self.shapeFrame = ShapeFrame(self.tabWidgetLeft, self.propeller)
		self.tabWidgetLeft.addTab(self.shapeFrame, "Blade shape")
		
		self.profileFrame = ProfileFrame(self.tabWidgetLeft, self.propeller)
		self.tabWidgetLeft.addTab(self.profileFrame, "Profile")
		
		self.settingsFrame = SettingsFrame(self.tabWidgetLeft, self.propeller)
		self.tabWidgetLeft.addTab(self.settingsFrame, "Settings")
		
		self.vtkFrame = VTKFrame(self.splitter, self.propeller)
		self.splitter.setSizes([720, 520])
		
		self.verticalLayout.addWidget(self.splitter)
		
		self.setCentralWidget(self.centralwidget)
		
		self.registerAll()
		
		self.menubar = QtGui.QMenuBar(self)
		self.menubar.setGeometry(QtCore.QRect(0, 0, 1280, 23))
		self.menubar.setObjectName("menubar")
		
		openAction = QtGui.QAction('&Open', self)
		openAction.setShortcut('Ctrl+O')
		openAction.setStatusTip('Open file')
		openAction.triggered.connect(self.loadFile)
		
		saveAction = QtGui.QAction('&Save', self)
		saveAction.setShortcut('Ctrl+S')
		saveAction.setStatusTip('Save file')
		saveAction.triggered.connect(self.saveFile)
		
		saveAsAction = QtGui.QAction('&Save as', self)
		saveAsAction.setShortcut('Ctrl+Shift+S')
		saveAsAction.setStatusTip('Save file as')
		saveAsAction.triggered.connect(self.saveFileAs)
		
		exportAction = QtGui.QAction('&Export', self)
		exportAction.setShortcut('Ctrl+E')
		exportAction.setStatusTip('Export as STL file')
		exportAction.triggered.connect(self.export)
		
		exitAction = QtGui.QAction('&Exit', self)
		exitAction.setShortcut('Ctrl+Q')
		exitAction.setStatusTip('Exit application')
		exitAction.triggered.connect(self.close)
		
		fileMenu = self.menubar.addMenu('&File')
		fileMenu.addAction(openAction)
		fileMenu.addAction(saveAction)
		fileMenu.addAction(saveAsAction)
		fileMenu.addAction(exportAction)
		fileMenu.addAction(exitAction)
		self.setMenuBar(self.menubar)
		
		self.statusbar = QtGui.QStatusBar(self)
		self.statusbar.setObjectName("statusbar")
		self.setStatusBar(self.statusbar)
		
		self.show()
	
	def updateAll(self):
		print 'Registering propeller in frames'
		self.generatorLineFrame.setPropeller(self.propeller)
		self.shapeFrame.setPropeller(self.propeller)
		self.profileFrame.setPropeller(self.propeller)
		self.settingsFrame.setPropeller(self.propeller)
		self.vtkFrame.setPropeller(self.propeller)
		
		self.registerAll()
	
	def registerAll(self):
		print 'Registering listeners'
		self.propeller.registerListener(self.generatorLineFrame)
		self.propeller.registerListener(self.shapeFrame)
		self.propeller.registerListener(self.profileFrame)
		self.propeller.registerListener(self.settingsFrame)
		self.propeller.registerListener(self.vtkFrame)
	
	def loadFile(self):
		fname = QtGui.QFileDialog.getOpenFileName(self, 'Open file', os.getcwd())
		if fname != '':
			self.propeller = PM.Model.Propeller.initializeFromFile(fname)
			self.currentPath = fname
			self.updateAll()
	
	def saveFile(self):
		if self.currentPath == None:
			self.saveFileAs()
		else:
			self.propeller.saveToFile(self.currentPath)
	
	def saveFileAs(self):
		fname = QtGui.QFileDialog.getSaveFileName(self, 'Open file', os.getcwd())
		if fname != '':
			self.propeller.saveToFile(fname)
			self.currentPath = fname
	
	def export(self):
		fname = QtGui.QFileDialog.getSaveFileName(self, 'Save file', os.getcwd())
		if fname != '':
			separate = False
			if '_.' in fname:
				separate = True
			if '.txt' not in fname and '.stl' not in fname:
				fname += '.stl'
			
			fileFormat = PM.Basis.Surface.FORMAT_STL
			if '.txt' in fname:
				fileFormat = PM.Basis.Surface.FORMAT_TXT
			
			self.propeller.export(fname, separate, fileFormat)
