from pyface.qt import QtGui

from traits.api import HasTraits, Instance, on_trait_change
from traitsui.api import View, Item
from mayavi.core.ui.mayavi_scene import MayaviScene
from tvtk.pyface.scene_editor import SceneEditor
from mayavi.tools.mlab_scene_model import MlabSceneModel

from PropellerModel.Model import Propeller

class VTKFrame(QtGui.QFrame):
	
	scene = Instance(MlabSceneModel, ())
	view = View(Item('scene',
	            width=620,
	            show_label=False,
	            editor=SceneEditor(scene_class=MayaviScene)),
	            resizable=True)
	
	def __init__(self, parent, propeller):
		super(VTKFrame, self).__init__(parent)
		
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
		sizePolicy.setHorizontalStretch(0)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
		self.setSizePolicy(sizePolicy)
		
		self.setFrameShape(QtGui.QFrame.StyledPanel)
		self.setFrameShadow(QtGui.QFrame.Raised)
		self.setObjectName("topRightFrame")
		
		self.verticalLayout = QtGui.QVBoxLayout(self)
		self.verticalLayout.setSpacing(1)
		self.verticalLayout.setContentsMargins(1, 1, 1, 1)
		self.verticalLayout.setObjectName("verticalLayout")
		
		self.frame = QtGui.QFrame(self)
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
		sizePolicy.setHorizontalStretch(0)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.frame.sizePolicy().hasHeightForWidth())
		self.frame.setSizePolicy(sizePolicy)
		self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
		self.frame.setFrameShadow(QtGui.QFrame.Raised)
		self.frame.setObjectName("frame")
		
		self.horizontalLayout = QtGui.QHBoxLayout(self.frame)
		self.horizontalLayout.setSpacing(1)
		self.horizontalLayout.setContentsMargins(1, 1, 1, 1)
		self.horizontalLayout.setObjectName("horizontalLayout")
		self.basisCheckBox = QtGui.QCheckBox(self.frame)
		
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
		sizePolicy.setHorizontalStretch(0)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.basisCheckBox.sizePolicy().hasHeightForWidth())
		
		self.basisCheckBox.setSizePolicy(sizePolicy)
		self.basisCheckBox.setChecked(False)
		self.basisCheckBox.setObjectName("basisCheckBox")
		self.basisCheckBox.setText("Basis")
		self.basisCheckBox.stateChanged.connect(self.updateVisibility)
		self.horizontalLayout.addWidget(self.basisCheckBox)
		
		self.PSCheckBox = QtGui.QCheckBox(self.frame)
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
		sizePolicy.setHorizontalStretch(0)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.PSCheckBox.sizePolicy().hasHeightForWidth())
		self.PSCheckBox.setSizePolicy(sizePolicy)
		self.PSCheckBox.setChecked(True)
		self.PSCheckBox.setObjectName("PSCheckBox")
		self.PSCheckBox.setText("Pressure side")
		self.PSCheckBox.stateChanged.connect(self.updateVisibility)
		self.horizontalLayout.addWidget(self.PSCheckBox)
		
		self.SSCheckBox = QtGui.QCheckBox(self.frame)
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
		sizePolicy.setHorizontalStretch(0)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.SSCheckBox.sizePolicy().hasHeightForWidth())
		self.SSCheckBox.setSizePolicy(sizePolicy)
		self.SSCheckBox.setObjectName("SSCheckBox")
		self.SSCheckBox.setText("Suction side")
		self.SSCheckBox.setChecked(True)
		self.SSCheckBox.stateChanged.connect(self.updateVisibility)
		self.horizontalLayout.addWidget(self.SSCheckBox)
		
		spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
		self.horizontalLayout.addItem(spacerItem1)
		
		self.verticalLayout.addWidget(self.frame)
		
		# here create mayavi scene!!!
		self.MayaviView = Visualization(self, propeller)
		self.visualisation = self.MayaviView.edit_traits(parent=self, kind='subpanel').control
		self.verticalLayout.addWidget(self.visualisation)
		self.visualisation.setParent(self)
		
		#self.updateVisibility()
	
	def setPropeller(self, propeller):
		self.MayaviView.setPropeller(propeller)
		self.refresh(Propeller.EVT_Resample)
	
	def updateVisibility(self):
		self.MayaviView.basisMesh.visible = self.basisCheckBox.isChecked()
		self.MayaviView.pressureSideMesh.visible = self.PSCheckBox.isChecked()
		self.MayaviView.suctionSideMesh.visible = self.SSCheckBox.isChecked()
		#self.refresh(Propeller.EVT_DiscretisationRefreshed)
	
	def refresh(self, event):
		if event == Propeller.EVT_DiscretisationRefreshed:
			print 'Update point coordinates'
			self.MayaviView.update_plot()
		
		if event == Propeller.EVT_Resample:
			print 'Clear scene and create new objects'
			self.MayaviView.scene.mlab.clf()
			self.MayaviView.update_plot()

class Visualization(HasTraits):
	
	scene = Instance(MlabSceneModel, ())
	view = View(Item('scene',
	            width=400,
	            show_label=False,
	            editor=SceneEditor(scene_class=MayaviScene)),
	            resizable=True)
	
	def __init__(self, parent, propeller):
		HasTraits.__init__(self)
		self.parent = parent
		self.propeller = propeller
		self.showBasis = False
		self.showPressureSide = True
		self.showSuctionSide = True
	
	def setPropeller(self, prop):
		self.propeller = prop

	@on_trait_change('scene.activated')
	def update_plot(self):
		print 'i:', len(self.propeller.basis.x), '\tj:', len(self.propeller.basis.x[0])
		self.basisMesh = self.scene.mlab.mesh(self.propeller.basis.x, self.propeller.basis.y, self.propeller.basis.z, color=(0,0,1), name='Basis', representation='wireframe')
		self.basisMesh.visible = self.parent.basisCheckBox.isChecked()
		
		self.pressureSideMesh = self.scene.mlab.mesh(self.propeller.pressureSide.x, self.propeller.pressureSide.y, self.propeller.pressureSide.z, color=(1,0,0), name='Pressure Side')
		self.pressureSideMesh.visible = self.parent.PSCheckBox.isChecked()
		
		self.suctionSideMesh = self.scene.mlab.mesh(self.propeller.suctionSide.x, self.propeller.suctionSide.y, self.propeller.suctionSide.z, color=(0,1,0), name='Suction Side')
		self.suctionSideMesh.visible = self.parent.SSCheckBox.isChecked()
