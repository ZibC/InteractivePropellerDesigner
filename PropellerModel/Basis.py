import numpy as np
from scipy.interpolate import interp1d, InterpolatedUnivariateSpline

class Curve():
	
	def __init__(self, sampling, xValues=None, yValues=None):
		self.sampling = sampling
		self.x = [0, 1]
		self.y = [0, 0]
		if xValues != None:
			self.x = xValues
		if yValues != None:
			self.y = yValues
	
	def set(self, x, y):
		if x in self.x:
			index = 0
			for i in range(len(self.x)):
				if self.x[i] == x:
					index = i
					break
			self.x[index] = x
			self.y[index] = y
		else:
			index = len(self.x)
			for i in range(index):
				if (self.x[i] > x):
					index = i
					break
			self.x.insert(index, x)
			self.y.insert(index, y)
	
	def get(self, x):
		if x < min(self.x):
			return self.y[0]
		elif x > max(self.x):
			return self.y[-1]
		else:
			if len(self.x) == 1:
				kind = 0
			elif len(self.x) == 2:
				kind = 1
			elif len(self.x) == 3:
				kind = 2
			else:
				kind = 3
			#f = interp1d(self.x, self.y, kind=kind)
			f = InterpolatedUnivariateSpline(self.x, self.y, k=kind)
			if type(x) is float:
				return float(f(x))
			else:
				return f(x)
	
	def resample(self):
		if len(self.x) == 1:
			kind = 0
		elif len(self.x) == 2:
			kind = 1
		elif len(self.x) == 3:
			kind = 2
		else:
			kind = 3
		xVals = np.linspace(min(self.x),1,self.sampling)
		f = interp1d(self.x, self.y, kind=kind)
		#f = InterpolatedUnivariateSpline(self.x, self.y, k=kind)
		yVals = f(xVals)
		return xVals, yVals
	
	@staticmethod
	def getCurvefromConfig(config, sectionName, sampling):
		xList = config.get(sectionName, 'XValues').replace('{', '').replace('}', '').strip().split(',')
		yList = config.get(sectionName, 'YValues').replace('{', '').replace('}', '').strip().split(',')
		x = []
		y = []
		for v in xList:
			x.append(float(v.strip()))
		for v in yList:
			y.append(float(v.strip()))
		return Curve(sampling, x, y)
	
	def save(self, config, sectionName):
		xString = '{'
		yString = '{'
		for i, v in enumerate(self.x):
			prefix = ','
			if i == 0:
				prefix = ''
			xString += prefix+' {0:.5e}'.format(v)
			yString += prefix+' {0:.5e}'.format(self.y[i])
		xString += ' }'
		yString += ' }'
		config.add_section(sectionName)
		config.set(sectionName, 'XValues', xString)
		config.set(sectionName, 'YValues', yString)

class Surface():
	
	FORMAT_STL = 0
	FORMAT_TXT = 1
	
	def __init__(self, nR, nC):
		self.nR = nR
		self.nC = nC
		self.x = []
		self.y = []
		self.z = []
		self.reset
	
	def setSampling(self, nR, nC):
		self.nR = nR
		self.nC = nC
	
	def reset(self):
		self.x = []
		self.y = []
		self.z = []
		for _ in range(self.nR):
			self.x.append(np.zeros(self.nC))
			self.y.append(np.zeros(self.nC))
			self.z.append(np.zeros(self.nC))
	
	def set(self, i, j, x, y, z):
		self.x[i][j] = x
		self.y[i][j] = y
		self.z[i][j] = z
	
	def getPoint(self, i, j):
		return np.array([self.x[i][j], self.y[i][j], self.z[i][j]])
	
	def export(self, objectName, factor, filename, fileFormat, mode='a'):
		if fileFormat == self.FORMAT_TXT:
			self.exportToTxt(factor, filename, mode)
		elif fileFormat == self.FORMAT_STL:
			self.exportToSTL(objectName, factor, filename, mode)
		else:
			print 'Unknown file format'
	
	def exportToTxt(self, factor, filename, mode='a'):
		fileObject = open(filename, mode)
		fileObject.write('{0} {1}\n'.format(self.nR, self.nC))
		for i in range(0, self.nR):
			for j in range(0, self.nC):
				fileObject.write('{0:.6e} {1:.6e} {2:.6e}\n'.format(self.x[i][j]*factor, self.y[i][j]*factor, self.z[i][j]*factor))
		fileObject.close()
	
	def exportToSTL(self, objectName, factor, filename, mode='a'):
		fileObject = open(filename, mode)
		fileObject.write('solid '+objectName+'\n')
		# facet loop
		for i in range(self.nR-1):
			for j in range(self.nC-1):
				# Calculate facet normals
				point1 = np.array(self.getPoint(i, j))*factor
				point2 = np.array(self.getPoint(i, j+1))*factor
				point3 = np.array(self.getPoint(i+1, j+1))*factor
				point4 = np.array(self.getPoint(i+1, j))*factor
				normalFacet1 = np.cross(point2 - point1, point3 - point2)
				normalFacet1 /= np.linalg.norm(normalFacet1)
				normalFacet2 = np.cross(point3 - point1, point4 - point1)
				normalFacet2 /= np.linalg.norm(normalFacet2)
				# Write facet 1
				fileObject.write('\tfacet normal {0:.6e} {1:.6e} {2:.6e}\n'.format(*normalFacet1))
				fileObject.write('\t\touter loop\n')
				fileObject.write('\t\t\tvertex {0:.6e} {1:.6e} {2:.6e}\n'.format(*point1))
				fileObject.write('\t\t\tvertex {0:.6e} {1:.6e} {2:.6e}\n'.format(*point2))
				fileObject.write('\t\t\tvertex {0:.6e} {1:.6e} {2:.6e}\n'.format(*point3))
				fileObject.write('\t\tendloop\n')
				fileObject.write('\tendfacet\n')
				# Write facet 2
				fileObject.write('\tfacet normal {0:.6e} {1:.6e} {2:.6e}\n'.format(*normalFacet2))
				fileObject.write('\t\touter loop\n')
				fileObject.write('\t\t\tvertex {0:.6e} {1:.6e} {2:.6e}\n'.format(*point1))
				fileObject.write('\t\t\tvertex {0:.6e} {1:.6e} {2:.6e}\n'.format(*point3))
				fileObject.write('\t\t\tvertex {0:.6e} {1:.6e} {2:.6e}\n'.format(*point4))
				fileObject.write('\t\tendloop\n')
				fileObject.write('\tendfacet\n')
		fileObject.write('endsolid '+objectName)
		fileObject.close()
