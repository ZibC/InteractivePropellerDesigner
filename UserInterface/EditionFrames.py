from PyQt4 import QtCore, QtGui

import numpy as np
from types import NoneType

from matplotlib.backends.backend_qt4agg import (FigureCanvasQTAgg as FigureCanvas, NavigationToolbar2QT as NavigationToolbar)
from matplotlib.figure import Figure

from PropellerModel.Model import Propeller

class GeneratorLineFrame(QtGui.QFrame):
	
	def __init__(self, parent, propeller):
		super(GeneratorLineFrame, self).__init__(parent)
		self.propeller = propeller
		self.mplCurves = None
		
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
		sizePolicy.setHorizontalStretch(0)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
		self.setSizePolicy(sizePolicy)
		self.setFrameShape(QtGui.QFrame.StyledPanel)
		self.setFrameShadow(QtGui.QFrame.Raised)
		self.setObjectName("topLeftFrame")
				
		self.gridLayout = QtGui.QGridLayout(self)
		self.gridLayout.setSpacing(2)
		self.gridLayout.setContentsMargins(1, 1, 1, 1)
		self.gridLayout.setObjectName("gridLayout")
		
		self.GlobalCurvesComboBox = QtGui.QComboBox(self)
		self.GlobalCurvesComboBox.addItems(['p/d', 'Chord', 'Skew', 'Rake'])
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
		sizePolicy.setHorizontalStretch(0)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.GlobalCurvesComboBox.sizePolicy().hasHeightForWidth())
		self.GlobalCurvesComboBox.setSizePolicy(sizePolicy)
		self.GlobalCurvesComboBox.setObjectName("GlobalCurvesComboBox")
		self.GlobalCurvesComboBox.activated[int].connect(self.changeCurve)
		self.GlobalCurvesComboBox.setCurrentIndex(0)
		
		self.gridLayout.addWidget(self.GlobalCurvesComboBox, 0, 0, 1, 1)
		
		self.frame = QtGui.QFrame(self)
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
		sizePolicy.setHorizontalStretch(1)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.frame.sizePolicy().hasHeightForWidth())
		self.frame.setSizePolicy(sizePolicy)
		self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
		self.frame.setFrameShadow(QtGui.QFrame.Raised)
		self.frame.setObjectName("frame")
		
		self.horizontalLayout = QtGui.QHBoxLayout(self.frame)
		self.horizontalLayout.setSpacing(1)
		self.horizontalLayout.setContentsMargins(1, 1, 1, 1)
		self.horizontalLayout.setObjectName("horizontalLayout")
		
		self.PDCheckBox = QtGui.QCheckBox(self.frame)
		self.PDCheckBox.setChecked(True)
		self.PDCheckBox.setObjectName("PDCheckBox")
		self.PDCheckBox.setText('P/D')
		self.PDCheckBox.stateChanged.connect(self.drawPlot)
		self.horizontalLayout.addWidget(self.PDCheckBox)
		
		self.ChordCheckBox = QtGui.QCheckBox(self.frame)
		self.ChordCheckBox.setChecked(True)
		self.ChordCheckBox.setObjectName("ChordCheckBox")
		self.ChordCheckBox.setText("Chord")
		self.ChordCheckBox.stateChanged.connect(self.drawPlot)
		self.horizontalLayout.addWidget(self.ChordCheckBox)
		
		self.SkewCheckBox = QtGui.QCheckBox(self.frame)
		self.SkewCheckBox.setChecked(True)
		self.SkewCheckBox.setObjectName("SkewCheckBox")
		self.SkewCheckBox.setText("Skew")
		self.SkewCheckBox.stateChanged.connect(self.drawPlot)
		self.horizontalLayout.addWidget(self.SkewCheckBox)
		
		self.RakeCheckBox = QtGui.QCheckBox(self.frame)
		self.RakeCheckBox.setChecked(True)
		self.RakeCheckBox.setObjectName("RakeCheckBox")
		self.RakeCheckBox.setText("Rake")
		self.RakeCheckBox.stateChanged.connect(self.drawPlot)
		self.horizontalLayout.addWidget(self.RakeCheckBox)
		
		spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
		self.horizontalLayout.addItem(spacerItem)
		
		self.gridLayout.addWidget(self.frame, 0, 1, 1, 1)
		
		self.buttonList = []
		self.CurveTable = QtGui.QTableWidget(self)
		self.CurveTable.setMaximumSize(QtCore.QSize(170, 16777215))
		self.CurveTable.setObjectName("CurveTable")
		self.CurveTable.setColumnCount(3)
		self.CurveTable.setColumnWidth(0,55)
		self.CurveTable.setColumnWidth(1,75)
		self.CurveTable.verticalHeader().setVisible(False)
		self.CurveTable.sizePolicy().setHorizontalStretch(0)
		self.CurveTable.sizePolicy().setVerticalStretch(1)
		self.CurveTable.cellChanged[int, int].connect(self.itemChanged)
		self.gridLayout.addWidget(self.CurveTable, 1, 0, 1, 1)
		
		self.changeCurve(self.GlobalCurvesComboBox.currentIndex())
		self.updateCurveTable()
		
		self.mplFrame = QtGui.QFrame(self)
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
		sizePolicy.setHorizontalStretch(1)
		sizePolicy.setVerticalStretch(1)
		self.mplFrame.setSizePolicy(sizePolicy)
		self.mplFrame.setObjectName("mplFrame")
		
		self.plot = Figure()
		
		self.mplCurves = FigureCanvas(self.plot)
		self.mplCurves.setParent(self.mplFrame)
		self.mplCurves.sizePolicy().setVerticalStretch(1)
		self.mplCurves.sizePolicy().setHorizontalStretch(1)
		self.dragPoint = False
		self.mplCurves.mpl_connect('pick_event', self.onClick)
		self.mplCurves.mpl_connect('motion_notify_event', self.onDrag)
		self.mplCurves.mpl_connect('button_release_event', self.onRelease)
		self.mplToolbar = NavigationToolbar(self.mplCurves, self.mplFrame, coordinates=True)
		
		LVBoxMplFrame = QtGui.QVBoxLayout()
		LVBoxMplFrame.addWidget(self.mplCurves)
		LVBoxMplFrame.addWidget(self.mplToolbar)
		LVBoxMplFrame.setSpacing(1)
		LVBoxMplFrame.setContentsMargins(1, 1, 1, 1)
		
		self.mplFrame.setLayout(LVBoxMplFrame)
		
		self.gridLayout.addWidget(self.mplFrame, 1, 1, 1, 1)
		
		self.drawPlot()
	
	def setPropeller(self, prop):
		self.propeller = prop
		self.changeCurve(self.selectedCurveIndex)
		self.updateCurveTable()
	
	def onClick(self, event):
		self.dragPoint = True
		xdata, _ = event.artist.get_data()
		ind = int(event.ind)
		self.selectedPointX = xdata[ind]

	def onDrag(self, event):
		if self.dragPoint:
			self.selectedCurve.set(self.selectedPointX, event.ydata)
			self.updateCurveTable()

	def onRelease(self, event):
		self.dragPoint = False
		self.propeller.refreshSurfaces()

	def itemChanged(self, i, j):
		if not self.updatingTable and i != self.CurveTable.rowCount()-1:
			if j == 0:
				self.updateCurveTable()
			elif i != self.CurveTable.rowCount()-1:
				self.selectedCurve.set(float(self.CurveTable.item(i,0).text()), float(self.CurveTable.item(i,1).text()))
				self.propeller.refreshSurfaces()
				self.updateCurveTable()

	def changeCurve(self, i):
		self.selectedCurveIndex = i
		self.selectedCurve = self.propeller.pd
		if i == 1:
			self.selectedCurve = self.propeller.chord
		if i == 2:
			self.selectedCurve = self.propeller.skew
		if i == 3:
			self.selectedCurve = self.propeller.rake
		self.updateCurveTable()

	def updateCurveTable(self):
		self.updatingTable = True
		self.buttonList = []
		self.CurveTable.clear()
		self.CurveTable.setHorizontalHeaderLabels(['r/R', 'Value', '+/-'])
		self.CurveTable.horizontalHeader().setStretchLastSection(True)
		
		self.CurveTable.setRowCount(len(self.selectedCurve.x)+1)
		for i in range(len(self.selectedCurve.x)):
			item = QtGui.QTableWidgetItem('{0:.3f}'.format(self.selectedCurve.x[i]))
			self.CurveTable.setItem(i, 0, item)
			item = QtGui.QTableWidgetItem('{0:.5f}'.format(self.selectedCurve.y[i]))
			self.CurveTable.setItem(i, 1, item)
			delButton = QtGui.QPushButton(self.CurveTable)
			delButton.setText('-')
			delButton.clicked.connect(self.deletePoint)
			self.buttonList.append(delButton)
			self.CurveTable.setCellWidget(i, 2, delButton)
		
		addButton = QtGui.QPushButton(self.CurveTable)
		addButton.setText('+')
		addButton.clicked.connect(self.addPoint)
		self.CurveTable.setCellWidget(i+1, 2, addButton)
		self.drawPlot()
		self.updatingTable = False

	def deletePoint(self):
		button = self.sender()
		localIndex = self.buttonList.index(button,0)
		del self.selectedCurve.x[localIndex]
		del self.selectedCurve.y[localIndex]
		self.propeller.refreshSurfaces()
		self.updateCurveTable()
	
	def addPoint(self):
		newX = float(self.CurveTable.item(self.CurveTable.rowCount()-1, 0).text())
		if type(self.CurveTable.item(self.CurveTable.rowCount()-1, 1)) != NoneType:
			newY = float(self.CurveTable.item(self.CurveTable.rowCount()-1, 1).text())
		else:
			newY = self.selectedCurve.get(newX)
		
		self.selectedCurve.set(newX, newY)
		self.propeller.refreshSurfaces()
		self.updateCurveTable()
	
	def drawPlot(self):
		if self.mplCurves != None:
			self.plot.clear()
			axis = self.plot.add_subplot(111)
			lineThickness = 1
			pick = None
			if self.PDCheckBox.isChecked():
				if self.selectedCurveIndex == 0:
					lineThickness = 2
					pick = 5
				else:
					lineThickness = 1
					pick = None
				axis.plot(self.propeller.pd.x, self.propeller.pd.y, color='red', marker='s', linestyle='', picker=pick)
				hdx, hdy = self.propeller.pd.resample()
				axis.plot(hdx, hdy, color='red', marker='', linestyle='-', linewidth=lineThickness)
			
			if self.ChordCheckBox.isChecked():
				if self.selectedCurveIndex == 1:
					lineThickness = 2
					pick = 5
				else:
					lineThickness = 1
					pick = None
				axis.plot(self.propeller.chord.x, self.propeller.chord.y, color='blue', marker='o', linestyle='', picker=pick)
				hdx, hdy = self.propeller.chord.resample()
				axis.plot(hdx, hdy, color='blue', marker='', linestyle='-', linewidth=lineThickness)
			
			if self.SkewCheckBox.isChecked():
				if self.selectedCurveIndex == 2:
					lineThickness = 2
					pick = 5
				else:
					lineThickness = 1
					pick = None
				axis.plot(self.propeller.skew.x, self.propeller.skew.y, color='green', marker='v', linestyle='', picker=pick)
				hdx, hdy = self.propeller.skew.resample()
				axis.plot(hdx, hdy, color='green', marker='', linestyle='-', linewidth=lineThickness)
			
			if self.RakeCheckBox.isChecked():
				if self.selectedCurveIndex == 3:
					lineThickness = 2
					pick = 5
				else:
					lineThickness = 1
					pick = None
				axis.plot(self.propeller.rake.x, self.propeller.rake.y, color='brown', marker='*', linestyle='', picker=pick)
				hdx, hdy = self.propeller.rake.resample()
				axis.plot(hdx, hdy, color='brown', marker='', linestyle='-', linewidth=lineThickness)
			
			axis.set_xlim(0, 1.1)
			axis.grid()
			self.mplCurves.draw()
	
	def refresh(self, event):
		if event == Propeller.EVT_Resample or event == Propeller.EVT_CurvesChanged:
			self.changeCurve(self.selectedCurveIndex)

class ShapeFrame(QtGui.QFrame):
	
	def __init__(self, parent, propeller):
		super(ShapeFrame, self).__init__(parent)
		self.propeller = propeller
		self.mplCurves = None
		
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
		sizePolicy.setHorizontalStretch(0)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
		self.setSizePolicy(sizePolicy)
		self.setFrameShape(QtGui.QFrame.StyledPanel)
		self.setFrameShadow(QtGui.QFrame.Raised)
		self.setObjectName("topLeftFrame")
				
		self.gridLayout = QtGui.QGridLayout(self)
		self.gridLayout.setSpacing(2)
		self.gridLayout.setContentsMargins(1, 1, 1, 1)
		self.gridLayout.setObjectName("gridLayout")
		
		self.GlobalCurvesComboBox = QtGui.QComboBox(self)
		self.GlobalCurvesComboBox.addItems(['Leading edge', 'Trailing edge', 'Rake trailing edge'])
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
		sizePolicy.setHorizontalStretch(0)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.GlobalCurvesComboBox.sizePolicy().hasHeightForWidth())
		self.GlobalCurvesComboBox.setSizePolicy(sizePolicy)
		self.GlobalCurvesComboBox.setObjectName("GlobalCurvesComboBox")
		self.GlobalCurvesComboBox.activated[int].connect(self.changeCurve)
		self.GlobalCurvesComboBox.setCurrentIndex(0)
		
		self.gridLayout.addWidget(self.GlobalCurvesComboBox, 0, 0, 1, 1)
		
		self.frame = QtGui.QFrame(self)
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
		sizePolicy.setHorizontalStretch(1)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.frame.sizePolicy().hasHeightForWidth())
		self.frame.setSizePolicy(sizePolicy)
		self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
		self.frame.setFrameShadow(QtGui.QFrame.Raised)
		self.frame.setObjectName("frame")
		
		self.horizontalLayout = QtGui.QHBoxLayout(self.frame)
		self.horizontalLayout.setSpacing(1)
		self.horizontalLayout.setContentsMargins(1, 1, 1, 1)
		self.horizontalLayout.setObjectName("horizontalLayout")
		
		self.updateButton = QtGui.QPushButton('Update geometry', self.frame)
		self.updateButton.clicked.connect(self.updatePropellerCurves)
		self.horizontalLayout.addWidget(self.updateButton)
		
		spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
		self.horizontalLayout.addItem(spacerItem)
		
		self.gridLayout.addWidget(self.frame, 0, 1, 1, 1)
		
		self.buttonList = []
		self.CurveTable = QtGui.QTableWidget(self)
		self.CurveTable.setMaximumSize(QtCore.QSize(170, 16777215))
		self.CurveTable.setObjectName("CurveTable")
		self.CurveTable.setColumnCount(3)
		self.CurveTable.setColumnWidth(0,55)
		self.CurveTable.setColumnWidth(1,75)
		self.CurveTable.verticalHeader().setVisible(False)
		self.CurveTable.sizePolicy().setHorizontalStretch(0)
		self.CurveTable.sizePolicy().setVerticalStretch(1)
		self.CurveTable.cellChanged[int, int].connect(self.itemChanged)
		self.gridLayout.addWidget(self.CurveTable, 1, 0, 1, 1)
		
		self.changeCurve(self.GlobalCurvesComboBox.currentIndex())
		self.updateCurveTable()
		
		self.mplFrame = QtGui.QFrame(self)
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
		sizePolicy.setHorizontalStretch(1)
		sizePolicy.setVerticalStretch(1)
		self.mplFrame.setSizePolicy(sizePolicy)
		self.mplFrame.setObjectName("mplFrame")
		
		self.plot = Figure()
		
		self.mplCurves = FigureCanvas(self.plot)
		self.mplCurves.setParent(self.mplFrame)
		self.mplCurves.sizePolicy().setVerticalStretch(1)
		self.mplCurves.sizePolicy().setHorizontalStretch(1)
		self.dragPoint = False
		self.mplCurves.mpl_connect('pick_event', self.onClick)
		self.mplCurves.mpl_connect('motion_notify_event', self.onDrag)
		self.mplCurves.mpl_connect('button_release_event', self.onRelease)
		self.mplToolbar = NavigationToolbar(self.mplCurves, self.mplFrame, coordinates=True)
		
		LVBoxMplFrame = QtGui.QVBoxLayout()
		LVBoxMplFrame.addWidget(self.mplCurves)
		LVBoxMplFrame.addWidget(self.mplToolbar)
		LVBoxMplFrame.setSpacing(1)
		LVBoxMplFrame.setContentsMargins(1, 1, 1, 1)
		
		self.mplFrame.setLayout(LVBoxMplFrame)
		
		self.gridLayout.addWidget(self.mplFrame, 1, 1, 1, 1)
		
		self.drawPlot()
	
	def updatePropellerCurves(self):
		self.propeller.updateCurvesFromShapeLines()
	
	def setPropeller(self, prop):
		self.propeller = prop
		self.changeCurve(self.selectedCurveIndex)
		self.updateCurveTable()
	
	def onClick(self, event):
		self.dragPoint = True
		xdata, ydata = event.artist.get_data()
		ind = int(event.ind)
		if self.selectedCurveIndex < 2:
			self.selectedR = np.sqrt(np.power(xdata[ind], 2) + np.power(ydata[ind], 2))
		else:
			self.selectedR = ydata[ind]

	def onDrag(self, event):
		if self.dragPoint:
			if self.selectedCurveIndex < 2:
				localR = np.sqrt(np.power(event.xdata, 2) + np.power(event.ydata, 2))
				theta = np.degrees(np.arccos(event.xdata/localR))
				if event.ydata < 0:
					theta = 360-theta
				self.selectedCurve.set(self.selectedR, theta)
			else:
				self.selectedCurve.set(self.selectedR, event.xdata)
			self.updateCurveTable()

	def onRelease(self, event):
		self.dragPoint = False

	def itemChanged(self, i, j):
		if not self.updatingTable and i != self.CurveTable.rowCount()-1:
			if j == 0:
				self.updateCurveTable()
			elif i != self.CurveTable.rowCount()-1:
				self.selectedCurve.set(float(self.CurveTable.item(i,0).text()), float(self.CurveTable.item(i,1).text()))
				self.updateCurveTable()

	def changeCurve(self, i):
		self.selectedCurveIndex = i
		self.selectedCurve = self.propeller.leadingEdge
		if i == 1:
			self.selectedCurve = self.propeller.trailingEdge
		if i == 2:
			self.selectedCurve = self.propeller.rakeOutline
		self.updateCurveTable()

	def updateCurveTable(self):
		self.updatingTable = True
		self.buttonList = []
		self.CurveTable.clear()
		self.CurveTable.setHorizontalHeaderLabels(['r/R', 'Value', '+/-'])
		self.CurveTable.horizontalHeader().setStretchLastSection(True)
		
		self.CurveTable.setRowCount(len(self.selectedCurve.x)+1)
		for i in range(len(self.selectedCurve.x)):
			item = QtGui.QTableWidgetItem('{0:.3f}'.format(self.selectedCurve.x[i]))
			self.CurveTable.setItem(i, 0, item)
			item = QtGui.QTableWidgetItem('{0:.5f}'.format(self.selectedCurve.y[i]))
			self.CurveTable.setItem(i, 1, item)
			delButton = QtGui.QPushButton(self.CurveTable)
			delButton.setText('-')
			delButton.clicked.connect(self.deletePoint)
			self.buttonList.append(delButton)
			self.CurveTable.setCellWidget(i, 2, delButton)
		
		addButton = QtGui.QPushButton(self.CurveTable)
		addButton.setText('+')
		addButton.clicked.connect(self.addPoint)
		self.CurveTable.setCellWidget(i+1, 2, addButton)
		self.drawPlot()
		self.updatingTable = False

	def deletePoint(self):
		button = self.sender()
		localIndex = self.buttonList.index(button,0)
		del self.selectedCurve.x[localIndex]
		del self.selectedCurve.y[localIndex]
		self.updateCurveTable()
	
	def addPoint(self):
		newX = float(self.CurveTable.item(self.CurveTable.rowCount()-1, 0).text())
		if type(self.CurveTable.item(self.CurveTable.rowCount()-1, 1)) != NoneType:
			newY = float(self.CurveTable.item(self.CurveTable.rowCount()-1, 1).text())
		else:
			newY = self.selectedCurve.get(newX)
		
		self.selectedCurve.set(newX, newY)
		self.updateCurveTable()
	
	def drawPlot(self):
		if self.mplCurves != None:
			self.plot.clear()
			axis = self.plot.add_subplot(111)
			lineThickness = 1
			pick = None
			
			if self.selectedCurveIndex == 0:
				lineThickness = 2
				pick = 5
			else:
				lineThickness = 1
				pick = None
			curve = self.propeller.leadingEdge
			xList = []
			yList = []
			for i,r in enumerate(curve.x):
				theta = curve.y[i]
				xList.append(r*np.cos(np.radians(theta)))
				yList.append(r*np.sin(np.radians(theta)))
			axis.plot(xList, yList, color='red', marker='s', linestyle='', picker=pick)
			hdx = []
			hdy = []
			rList, thetaList = curve.resample()
			for i,r in enumerate(rList):
				theta = thetaList[i]
				hdx.append(r*np.cos(np.radians(theta)))
				hdy.append(r*np.sin(np.radians(theta)))
			axis.plot(hdx, hdy, color='red', marker='', linestyle='-', linewidth=lineThickness)
			
			if self.selectedCurveIndex == 1:
				lineThickness = 2
				pick = 5
			else:
				lineThickness = 1
				pick = None
			curve = self.propeller.trailingEdge
			xList = []
			yList = []
			for i,r in enumerate(curve.x):
				theta = curve.y[i]
				xList.append(r*np.cos(np.radians(theta)))
				yList.append(r*np.sin(np.radians(theta)))
			axis.plot(xList, yList, color='blue', marker='o', linestyle='', picker=pick)
			hdx = []
			hdy = []
			rList, thetaList = curve.resample()
			for i,r in enumerate(rList):
				theta = thetaList[i]
				hdx.append(r*np.cos(np.radians(theta)))
				hdy.append(r*np.sin(np.radians(theta)))
			axis.plot(hdx, hdy, color='blue', marker='', linestyle='-', linewidth=lineThickness)
			
			if self.selectedCurveIndex == 2:
				lineThickness = 2
				pick = 5
			else:
				lineThickness = 1
				pick = None
			curve = self.propeller.rakeOutline
			hdx, hdy = curve.resample()
			axis.plot(curve.y, curve.x, color='green', marker='v', linestyle='', picker=pick)
			axis.plot(hdy, hdx, color='green', marker='', linestyle='-', linewidth=lineThickness)
			
			axis.axis('equal')
			axis.set_xlim(-1.1, 1.1)
			axis.set_ylim(-1.1, 1.1)
			axis.grid()
			self.mplCurves.draw()
	
	def refresh(self, event):
		if event == Propeller.EVT_Resample:
			self.changeCurve(self.selectedCurveIndex)

class ProfileFrame(QtGui.QFrame):
	
	def __init__(self, parent, propeller):
		super(ProfileFrame, self).__init__(parent)
		self.propeller = propeller
		self.mplCurves = None
		
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
		sizePolicy.setHorizontalStretch(0)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
		self.setSizePolicy(sizePolicy)
		self.setFrameShape(QtGui.QFrame.StyledPanel)
		self.setFrameShadow(QtGui.QFrame.Raised)
		self.setObjectName("bottomLeftFrame")
		
		self.gridLayout = QtGui.QGridLayout(self)
		self.gridLayout.setSpacing(2)
		self.gridLayout.setContentsMargins(1, 1, 1, 1)
		self.gridLayout.setObjectName("gridLayout")
		
		self.ProfileDetailsComboBox = QtGui.QComboBox(self)
		self.ProfileDetailsComboBox.addItems(['Offsets', 'Thickness'])
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
		sizePolicy.setHorizontalStretch(0)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.ProfileDetailsComboBox.sizePolicy().hasHeightForWidth())
		self.ProfileDetailsComboBox.setSizePolicy(sizePolicy)
		self.ProfileDetailsComboBox.setObjectName("ProfileDetailsComboBox")
		self.ProfileDetailsComboBox.activated[int].connect(self.changeCurve)
		self.ProfileDetailsComboBox.setCurrentIndex(0)
		
		self.gridLayout.addWidget(self.ProfileDetailsComboBox, 0, 0, 1, 1)
		
		self.frame = QtGui.QFrame(self)
		self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
		self.frame.setFrameShadow(QtGui.QFrame.Raised)
		self.frame.setObjectName("frame")
		
		self.horizontalLayout = QtGui.QHBoxLayout(self.frame)
		self.horizontalLayout.setSpacing(1)
		self.horizontalLayout.setContentsMargins(1, 1, 1, 1)
		self.horizontalLayout.setObjectName("horizontalLayout")
		
		self.ProfileChoiceComboBox = QtGui.QComboBox(self.frame)
		items = []
		for key in sorted(self.propeller.profiles.DB.iterkeys()):
			items.append(key)
		self.ProfileChoiceComboBox.addItems(items)
		self.ProfileChoiceComboBox.setObjectName("ProfileChoiceComboBox")
		self.ProfileChoiceComboBox.setCurrentIndex(0)
		self.ProfileChoiceComboBox.activated[int].connect(self.changeProfile)
		self.horizontalLayout.addWidget(self.ProfileChoiceComboBox)
		
		self.delButton = QtGui.QPushButton(self.frame)
		self.delButton.setText('-')
		self.delButton.clicked.connect(self.deleteProfile)
		self.delButton.setMaximumSize(QtCore.QSize(20, 16777215))
		self.horizontalLayout.addWidget(self.delButton)
		
		self.offsetsCheckBox = QtGui.QCheckBox(self.frame)
		self.offsetsCheckBox.setChecked(True)
		self.offsetsCheckBox.setObjectName("offsetsCheckBox")
		self.offsetsCheckBox.setText("Offsets")
		self.offsetsCheckBox.stateChanged.connect(self.drawPlot)
		self.horizontalLayout.addWidget(self.offsetsCheckBox)
		
		self.thicknessCheckBox = QtGui.QCheckBox(self.frame)
		self.thicknessCheckBox.setChecked(True)
		self.thicknessCheckBox.setObjectName("thicknessCheckBox")
		self.thicknessCheckBox.setText("Thickness")
		self.thicknessCheckBox.stateChanged.connect(self.drawPlot)
		self.horizontalLayout.addWidget(self.thicknessCheckBox)
		
		spacerItem2 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
		self.horizontalLayout.addItem(spacerItem2)
		
		self.profileText = QtGui.QLineEdit(self.frame)
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
		sizePolicy.setHorizontalStretch(0)
		sizePolicy.setVerticalStretch(0)
		self.profileText.setMaximumSize(QtCore.QSize(70, 16777215))
		self.profileText.setSizePolicy(sizePolicy)
		self.horizontalLayout.addWidget(self.profileText)
		
		self.addButton = QtGui.QPushButton(self.frame)
		self.addButton.setText('+')
		self.addButton.setMaximumSize(QtCore.QSize(20, 16777215))
		self.addButton.clicked.connect(self.addProfile)
		self.horizontalLayout.addWidget(self.addButton)
		
		self.frame.sizePolicy().setHorizontalStretch(1)
		self.frame.sizePolicy().setVerticalStretch(0)
		
		self.gridLayout.addWidget(self.frame, 0, 1, 1, 1)
		
		self.ProfileTable = QtGui.QTableWidget(self)
		self.ProfileTable.setMaximumSize(QtCore.QSize(170, 16777215))
		self.ProfileTable.setObjectName("ProfileTable")
		self.ProfileTable.setColumnCount(3)
		self.ProfileTable.setColumnWidth(0,55)
		self.ProfileTable.setColumnWidth(1,75)
		self.ProfileTable.verticalHeader().setVisible(False)
		self.ProfileTable.sizePolicy().setHorizontalStretch(0)
		self.ProfileTable.sizePolicy().setVerticalStretch(1)
		self.ProfileTable.cellChanged[int, int].connect(self.itemChanged)
		
		self.gridLayout.addWidget(self.ProfileTable, 1, 0, 1, 1)
		self.selectedCurveIndex = 0
		self.changeProfile(self.ProfileChoiceComboBox.currentIndex())
		self.updateCurveTable()
		
		self.mplFrame = QtGui.QFrame(self)
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
		sizePolicy.setHorizontalStretch(1)
		sizePolicy.setVerticalStretch(1)
		self.mplFrame.setSizePolicy(sizePolicy)
		self.mplFrame.setObjectName("mplFrame")
		
		self.plot = Figure()
		
		self.mplCurves = FigureCanvas(self.plot)
		self.mplCurves.setParent(self.mplFrame)
		self.mplCurves.sizePolicy().setVerticalStretch(1)
		self.mplCurves.sizePolicy().setHorizontalStretch(1)
		self.dragPoint = False
		self.mplCurves.mpl_connect('pick_event', self.onClick)
		self.mplCurves.mpl_connect('motion_notify_event', self.onDrag)
		self.mplCurves.mpl_connect('button_release_event', self.onRelease)
		self.mplToolbar = NavigationToolbar(self.mplCurves, self.mplFrame, coordinates=True)
		
		LVBoxMplFrame = QtGui.QVBoxLayout()
		LVBoxMplFrame.addWidget(self.mplCurves)
		LVBoxMplFrame.addWidget(self.mplToolbar)
		LVBoxMplFrame.setSpacing(1)
		LVBoxMplFrame.setContentsMargins(1, 1, 1, 1)
		
		self.mplFrame.setLayout(LVBoxMplFrame)
		
		self.gridLayout.addWidget(self.mplFrame, 1, 1, 1, 1)
		
		self.drawPlot()
	
	def setPropeller(self, prop):
		self.propeller = prop
		self.refresh(Propeller.EVT_ProfilesDBModified)
	
	def refresh(self, event):
		if event == Propeller.EVT_ProfilesDBModified:
			for i in reversed(range(self.ProfileChoiceComboBox.count())):
				self.ProfileChoiceComboBox.removeItem(i)
			items = []
			for key in sorted(self.propeller.profiles.DB.iterkeys()):
				items.append(key)
			if self.selectedProfileIndex >= len(items):
				self.selectedProfileIndex = 0
			self.ProfileChoiceComboBox.addItems(items)
			self.ProfileChoiceComboBox.setCurrentIndex(self.selectedProfileIndex)
			self.changeProfile(self.selectedProfileIndex)
		
		if event == Propeller.EVT_Resample:
			self.changeProfile(self.selectedProfileIndex)
	
	def onClick(self, event):
		self.dragPoint = True
		xdata, _ = event.artist.get_data()
		ind = int(event.ind)
		self.selectedPointX = xdata[ind]

	def onDrag(self, event):
		if self.dragPoint:
			self.selectedCurve.set(self.selectedPointX, event.ydata)
			self.updateCurveTable()

	def onRelease(self, event):
		self.dragPoint = False
		self.propeller.refreshSurfaces()

	def itemChanged(self, i, j):
		if not self.updatingTable and i != self.ProfileTable.rowCount()-1:
			if j == 0:
				self.updateCurveTable()
			elif i != self.ProfileTable.rowCount()-1:
				self.selectedCurve.set(float(self.ProfileTable.item(i,0).text()), float(self.ProfileTable.item(i,1).text()))
				self.propeller.refreshSurfaces()
				self.updateCurveTable()

	def changeCurve(self, i):
		self.selectedCurveIndex = i
		self.selectedCurve = self.selectedProfile.offsets
		if i == 1:
			self.selectedCurve = self.selectedProfile.thickness
		self.updateCurveTable()

	def changeProfile(self, i):
		self.selectedProfileIndex = i
		self.selectedProfile = self.propeller.profiles.get(float(self.ProfileChoiceComboBox.currentText()))
		self.changeCurve(self.selectedCurveIndex)

	def updateCurveTable(self):
		self.updatingTable = True
		self.buttonList = []
		self.ProfileTable.clear()
		self.ProfileTable.setHorizontalHeaderLabels(['x/c', 'Value', '+/-'])
		self.ProfileTable.horizontalHeader().setStretchLastSection(True)
		
		self.ProfileTable.setRowCount(len(self.selectedCurve.x)+1)
		for i in range(len(self.selectedCurve.x)):
			item = QtGui.QTableWidgetItem('{0:.3f}'.format(self.selectedCurve.x[i]))
			self.ProfileTable.setItem(i, 0, item)
			item = QtGui.QTableWidgetItem('{0:.5f}'.format(self.selectedCurve.y[i]))
			self.ProfileTable.setItem(i, 1, item)
			item = QtGui.QTableWidgetItem('-')
			self.ProfileTable.setItem(i, 2, item)
			delButton = QtGui.QPushButton(self.ProfileTable)
			delButton.setText('-')
			delButton.clicked.connect(self.deletePoint)
			self.buttonList.append(delButton)
			self.ProfileTable.setCellWidget(i, 2, delButton)
		
		addButton = QtGui.QPushButton(self.ProfileTable)
		addButton.setText('+')
		addButton.clicked.connect(self.addPoint)
		self.ProfileTable.setCellWidget(i+1, 2, addButton)
		self.drawPlot()
		self.updatingTable = False

	def deletePoint(self):
		button = self.sender()
		localIndex = self.buttonList.index(button,0)
		del self.selectedCurve.x[localIndex]
		del self.selectedCurve.y[localIndex]
		self.propeller.refreshSurfaces()
		self.updateCurveTable()
	
	def addPoint(self):
		newX = float(self.ProfileTable.item(self.ProfileTable.rowCount()-1, 0).text())
		if type(self.ProfileTable.item(self.ProfileTable.rowCount()-1, 1)) != NoneType:
			newY = float(self.ProfileTable.item(self.ProfileTable.rowCount()-1, 1).text())
		else:
			newY = self.selectedCurve.get(newX)
		self.selectedCurve.set(newX, newY)
		self.propeller.refreshSurfaces()
		self.updateCurveTable()
	
	def deleteProfile(self):
		text = str(self.ProfileChoiceComboBox.currentText()).strip()
		del self.propeller.profiles.DB[text]
		for i in reversed(range(self.ProfileChoiceComboBox.count())):
			self.ProfileChoiceComboBox.removeItem(i)
		items = []
		for key in sorted(self.propeller.profiles.DB.iterkeys()):
			items.append(key)
		self.ProfileChoiceComboBox.addItems(items)
		if self.selectedProfileIndex >= len(items):
			self.selectedProfileIndex = len(items)-1
		self.ProfileChoiceComboBox.setCurrentIndex(self.selectedProfileIndex)
	
	def addProfile(self):
		text = str(self.profileText.text()).strip()
		if text != '':
			r = float(text)
			self.propeller.profiles.add(r)
			
			for i in reversed(range(self.ProfileChoiceComboBox.count())):
				self.ProfileChoiceComboBox.removeItem(i)
			items = []
			i = 0
			for key in sorted(self.propeller.profiles.DB.iterkeys()):
				items.append(key)
				if float(key) == r:
					self.selectedProfileIndex = i
				i+=1
			self.ProfileChoiceComboBox.addItems(items)
			self.ProfileChoiceComboBox.setCurrentIndex(self.selectedProfileIndex)
			self.changeProfile(self.selectedProfileIndex)
	
	def drawPlot(self):
		if self.mplCurves != None:
			self.plot.clear()
			axis = self.plot.add_subplot(111)
			lineThickness = 1
			pick = None
			if self.offsetsCheckBox.isChecked():
				curve = self.selectedProfile.offsets
				if self.selectedCurveIndex == 0:
					lineThickness = 2
					pick = 5
				else:
					lineThickness = 1
					pick = None
				axis.plot(curve.x, curve.y, color='red', marker='s', linestyle='', picker=pick)
				hdx, hdy = curve.resample()
				axis.plot(hdx, hdy, color='red', marker='', linestyle='-', linewidth=lineThickness)
			
			if self.thicknessCheckBox.isChecked():
				curve = self.selectedProfile.thickness
				if self.selectedCurveIndex == 1:
					lineThickness = 2
					pick = 5
				else:
					lineThickness = 1
					pick = None
				axis.plot(curve.x, curve.y, color='green', marker='o', linestyle='', picker=pick)
				hdx, hdy = curve.resample()
				axis.plot(hdx, hdy, color='green', marker='', linestyle='-', linewidth=lineThickness)
			
			ix, iy = self.selectedProfile.offsets.resample()
			_ , ey = self.selectedProfile.thickness.resample()
			axis.axis('equal')
			axis.set_xlim(-0.1, 1.1)
			axis.grid()
			axis.plot(ix, iy+ey, color='red', marker='', linestyle=':', linewidth=1)
			
			self.mplCurves.draw()

class SettingsFrame(QtGui.QFrame):
	
	def __init__(self, parent, propeller):
		super(SettingsFrame, self).__init__(parent)
		self.propeller = propeller
		self.mplCurves = None
		
		self.verticalLayout = QtGui.QVBoxLayout(self)
		self.verticalLayout.setObjectName("verticalLayout")
		
		self.groupBox = QtGui.QGroupBox(self)
		self.groupBox.setTitle('Propeller Properties')
		
		self.gridLayout = QtGui.QGridLayout(self.groupBox)
		
		self.label = QtGui.QLabel(self.groupBox)
		self.label.setText("Diameter :")
		self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
		
		self.diameterText = QtGui.QLineEdit(self.groupBox)
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Fixed)
		sizePolicy.setHorizontalStretch(0)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.diameterText.sizePolicy().hasHeightForWidth())
		self.diameterText.setSizePolicy(sizePolicy)
		self.diameterText.returnPressed.connect(self.updateU)
		self.gridLayout.addWidget(self.diameterText, 0, 1, 1, 1)
		
		self.label_6 = QtGui.QLabel(self.groupBox)
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
		sizePolicy.setHorizontalStretch(1)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.label_6.sizePolicy().hasHeightForWidth())
		self.label_6.setSizePolicy(sizePolicy)
		self.label_6.setText("mm")
		self.gridLayout.addWidget(self.label_6, 0, 2, 1, 1)
		
		self.label_2 = QtGui.QLabel(self.groupBox)
		self.label_2.setText("Rpm ")
		self.gridLayout.addWidget(self.label_2, 1, 0, 1, 1)
		
		self.rpmText = QtGui.QLineEdit(self.groupBox)
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Fixed)
		sizePolicy.setHeightForWidth(self.rpmText.sizePolicy().hasHeightForWidth())
		self.rpmText.setSizePolicy(sizePolicy)
		self.rpmText.setText(str(self.propeller.rpm))
		self.rpmText.returnPressed.connect(self.updateU)
		self.gridLayout.addWidget(self.rpmText, 1, 1, 1, 1)
		
		self.label_7 = QtGui.QLabel(self.groupBox)
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
		sizePolicy.setHorizontalStretch(1)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.label_7.sizePolicy().hasHeightForWidth())
		self.label_7.setSizePolicy(sizePolicy)
		self.label_7.setText("tr/min")
		self.gridLayout.addWidget(self.label_7, 1, 2, 1, 1)
		
		self.label_3 = QtGui.QLabel(self.groupBox)
		self.label_3.setText("Speed :")
		self.gridLayout.addWidget(self.label_3, 2, 0, 1, 1)
		
		self.speedText = QtGui.QLineEdit(self.groupBox)
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Fixed)
		sizePolicy.setHorizontalStretch(0)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.speedText.sizePolicy().hasHeightForWidth())
		self.speedText.setSizePolicy(sizePolicy)
		self.speedText.returnPressed.connect(self.updateJ)
		self.gridLayout.addWidget(self.speedText, 2, 1, 1, 1)
		n = self.propeller.rpm/60.0
		u = self.propeller.J*self.propeller.diameter*n*3.6
		self.speedText.setText('{0:.2f}'.format(u))
		
		self.label_8 = QtGui.QLabel(self.groupBox)
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
		sizePolicy.setHorizontalStretch(1)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.label_8.sizePolicy().hasHeightForWidth())
		self.label_8.setSizePolicy(sizePolicy)
		self.label_8.setText("km/h")
		self.gridLayout.addWidget(self.label_8, 2, 2, 1, 1)
		
		self.label_4 = QtGui.QLabel(self.groupBox)
		self.label_4.setText("Slip ratio :")
		self.gridLayout.addWidget(self.label_4, 3, 0, 1, 1)
		
		self.slipRatioLabel = QtGui.QLabel(self.groupBox)
		self.slipRatioLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
		pd = np.average(self.propeller.pd.y)
		self.slipRatioLabel.setText('{0:.1f}'.format((1 - self.propeller.J / pd)*100))
		self.gridLayout.addWidget(self.slipRatioLabel, 3, 1, 1, 1)
		
		self.label_9 = QtGui.QLabel(self.groupBox)
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
		sizePolicy.setHorizontalStretch(1)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.label_9.sizePolicy().hasHeightForWidth())
		self.label_9.setSizePolicy(sizePolicy)
		self.label_9.setText("%")
		self.gridLayout.addWidget(self.label_9, 3, 2, 1, 1)
		
		self.verticalLayout.addWidget(self.groupBox)
		
		self.groupBox_2 = QtGui.QGroupBox(self)
		self.groupBox_2.setObjectName("groupBox_2")
		self.groupBox_2.setTitle('Display')
		
		self.gridLayout_2 = QtGui.QGridLayout(self.groupBox_2)
		self.gridLayout_2.setObjectName("gridLayout_2")
		
		self.label_10 = QtGui.QLabel(self.groupBox_2)
		self.label_10.setText("Radii")
		self.gridLayout_2.addWidget(self.label_10, 0, 0, 1, 1)
		
		self.radiiText = QtGui.QLineEdit(self.groupBox_2)
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Fixed)
		sizePolicy.setHeightForWidth(self.radiiText.sizePolicy().hasHeightForWidth())
		self.radiiText.setSizePolicy(sizePolicy)
		self.radiiText.returnPressed.connect(self.setNPoints)
		self.gridLayout_2.addWidget(self.radiiText, 0, 1, 1, 1)
		
		self.label_11 = QtGui.QLabel(self.groupBox_2)
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
		sizePolicy.setHorizontalStretch(1)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.label_11.sizePolicy().hasHeightForWidth())
		self.label_11.setSizePolicy(sizePolicy)
		self.label_11.setText("points")
		self.gridLayout_2.addWidget(self.label_11, 0, 2, 1, 1)
		
		self.label_12 = QtGui.QLabel(self.groupBox_2)
		self.label_12.setText("Chord")
		self.gridLayout_2.addWidget(self.label_12, 1, 0, 1, 1)
		
		self.chordText = QtGui.QLineEdit(self.groupBox_2)
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Fixed)
		sizePolicy.setHeightForWidth(self.chordText.sizePolicy().hasHeightForWidth())
		self.chordText.setSizePolicy(sizePolicy)
		self.chordText.setObjectName("chordText")
		self.chordText.returnPressed.connect(self.setNPoints)
		self.gridLayout_2.addWidget(self.chordText, 1, 1, 1, 1)
		
		self.label_13 = QtGui.QLabel(self.groupBox_2)
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
		sizePolicy.setHorizontalStretch(1)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.label_13.sizePolicy().hasHeightForWidth())
		self.label_13.setSizePolicy(sizePolicy)
		self.label_13.setText("points")
		self.gridLayout_2.addWidget(self.label_13, 1, 2, 1, 1)
		
		self.verticalLayout.addWidget(self.groupBox_2)
		
		self.groupBox_3 = QtGui.QGroupBox(self)
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
		sizePolicy.setHorizontalStretch(0)
		sizePolicy.setVerticalStretch(1)
		sizePolicy.setHeightForWidth(self.groupBox_3.sizePolicy().hasHeightForWidth())
		self.groupBox_3.setSizePolicy(sizePolicy)
		self.groupBox_3.setObjectName("groupBox_3")
		self.groupBox_3.setTitle('Angle of attack')
		
		self.verticalLayout_2 = QtGui.QVBoxLayout(self.groupBox_3)
		self.verticalLayout_2.setObjectName("verticalLayout_2")
		
		self.mplFrame = QtGui.QFrame(self.groupBox_3)
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
		sizePolicy.setHorizontalStretch(1)
		sizePolicy.setVerticalStretch(1)
		self.mplFrame.setSizePolicy(sizePolicy)
		self.mplFrame.setObjectName("mplFrame")
		
		self.plot = Figure()
		
		self.mplCurves = FigureCanvas(self.plot)
		self.mplCurves.setParent(self.mplFrame)
		self.mplCurves.sizePolicy().setVerticalStretch(1)
		self.mplCurves.sizePolicy().setHorizontalStretch(1)
		self.dragPoint = False
		self.mplToolbar = NavigationToolbar(self.mplCurves, self.mplFrame, coordinates=True)
		
		LVBoxMplFrame = QtGui.QVBoxLayout()
		LVBoxMplFrame.addWidget(self.mplCurves)
		LVBoxMplFrame.addWidget(self.mplToolbar)
		LVBoxMplFrame.setSpacing(1)
		LVBoxMplFrame.setContentsMargins(1, 1, 1, 1)
		
		self.mplFrame.setLayout(LVBoxMplFrame)
		
		self.verticalLayout_2.addWidget(self.mplFrame)
		
		self.verticalLayout.addWidget(self.groupBox_3)
		
		self.groupBox_4 = QtGui.QGroupBox(self)
		self.groupBox_4.setObjectName("groupBox_4")
		self.groupBox_4.setTitle('Profiles automatic adaptation')
		
		self.gridLayout_3 = QtGui.QGridLayout(self.groupBox_4)
		self.gridLayout_3.setObjectName("gridLayout_3")
		
		self.comboBox = QtGui.QComboBox(self.groupBox_4)
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
		sizePolicy.setHorizontalStretch(0)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.comboBox.sizePolicy().hasHeightForWidth())
		self.comboBox.setSizePolicy(sizePolicy)
		self.comboBox.setObjectName("comboBox")
		self.comboBox.addItems(['Tulin 2 terms', 'Johnson 3 terms'])
		self.gridLayout_3.addWidget(self.comboBox, 0, 0, 1, 1)
		
		self.checkBox = QtGui.QCheckBox(self.groupBox_4)
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
		sizePolicy.setHorizontalStretch(1)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.checkBox.sizePolicy().hasHeightForWidth())
		self.checkBox.setSizePolicy(sizePolicy)
		self.checkBox.setObjectName("checkBox")
		self.checkBox.setText('Correct zero angle')
		self.checkBox.setChecked(True)
		self.gridLayout_3.addWidget(self.checkBox, 0, 1, 1, 1)
		
		self.pushButton = QtGui.QPushButton('Apply', self.groupBox_4)
		sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
		sizePolicy.setHorizontalStretch(0)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.pushButton.sizePolicy().hasHeightForWidth())
		self.pushButton.setSizePolicy(sizePolicy)
		self.pushButton.setMaximumSize(QtCore.QSize(60, 16777215))
		self.pushButton.setObjectName("pushButton")
		self.pushButton.clicked.connect(self.adjustProfiles)
		self.gridLayout_3.addWidget(self.pushButton, 0, 2, 1, 1)
		
		self.verticalLayout.addWidget(self.groupBox_4)
		
		self.refresh()
	
	def setNPoints(self):
		nR = int(self.radiiText.text())
		nC = int(self.chordText.text())
		self.propeller.setSampling(nR, nC)
	
	def setPropeller(self, prop):
		self.propeller = prop
		self.refresh()
	
	def drawPlot(self):
		if self.mplCurves != None:
			self.plot.clear()
			axis = self.plot.add_subplot(111)
			r, a = self.propeller.getAngleOfAttackDistribution()
			axis.plot(r, a, '-b')
			axis.set_xlim(0, 1.1)
			axis.grid()
			self.mplCurves.draw()
	
	def refresh(self, event=None):
		self.diameterText.setText('{0:.1f}'.format(self.propeller.diameter*1000))
		self.rpmText.setText(str(self.propeller.rpm))
		self.speedText.setText('{0:.2f}'.format(self.propeller.J * self.propeller.diameter * self.propeller.rpm/60 * 3.6))
		pd = np.average(self.propeller.pd.y)
		self.slipRatioLabel.setText('{0:.1f}'.format((1 - self.propeller.J / pd)*100))
		self.radiiText.setText(str(self.propeller.nR))
		self.chordText.setText(str(self.propeller.nC))
		self.drawPlot()
	
	def updateJ(self):
		u = float(self.speedText.text())/3.6
		n = self.propeller.rpm/60
		D = self.propeller.diameter
		J = u/ (n*D)
		self.propeller.J = J
		self.refresh()
	
	def updateU(self):
		diam = float(self.diameterText.text())/1000.0
		print diam
		self.propeller.diameter = diam
		rpm = float(self.rpmText.text())
		self.propeller.rpm = rpm
		u = (rpm/60)*(diam)*self.propeller.J*3.6
		print u
		self.speedText.setText('{0:.2f}'.format(u))
	
	def adjustProfiles(self):
		self.propeller.adaptProfiles(self.comboBox.currentIndex(), self.checkBox.isChecked())
