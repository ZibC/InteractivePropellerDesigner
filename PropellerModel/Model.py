import numpy as np
import ConfigParser

from Basis import Curve, Surface
from RadiiDefinition import ProfilesDB, Profile

class Propeller():
	
	EVT_Resample = 0
	EVT_DiscretisationRefreshed = 1
	EVT_ProfilesDBModified = 2
	EVT_CurvesChanged = 3
	Events = ['EVT_Resample', 'EVT_DiscretisationRefreshed', 'EVT_ProfilesDBModified', 'EVT_CurvesChanged']
	
	def __init__(self, Diam, rSampling=17, cSampling=11, PDCurve=None, ChordCurve=None, SkewCurve=None, RakeCurve=None, Profiles=None, LeadingEdgeCurve=None, TrailingEdgeCurve=None, RakeOutlineCurve=None):
		self.diameter = Diam
		self.nR = rSampling
		self.nC = cSampling
		self.nT = 4
		self.basis = Surface(self.nR, self.nC)
		self.pressureSide = Surface(self.nR, self.nC)
		self.suctionSide = Surface(self.nR, self.nC)
		self.LE = Surface(self.nR, self.nT)
		self.TE = Surface(self.nR, self.nT)
		self.tip = Surface(self.nC, self.nT)
		
		self.pd = Curve(self.nR, [0.2, 1], [1, 1])
		self.chord = Curve(self.nR, [0.2, 1], [0.5, 0.5])
		self.skew = Curve(self.nR, [0.2, 1], [0, 0])
		self.rake = Curve(self.nR, [0.2, 1], [0, 0])
		self.profiles = ProfilesDB(self.nR, self.nC)
		
		self.rakeOutline = Curve(self.nR, [0.2, 1], [0.05, 0.2])
		self.leadingEdge = Curve(self.nR, [0.2, 1], [0, 45])
		self.trailingEdge = Curve(self.nR, [0.2, 1], [90, 90])
		self.rpm = 25000
		self.J = 0.85
		
		if PDCurve != None:
			self.pd = PDCurve
		
		if ChordCurve != None:
			self.chord = ChordCurve
		
		if SkewCurve != None:
			self.skew = SkewCurve
		
		if RakeCurve != None:
			self.rake = RakeCurve
		
		if Profiles != None:
			self.profiles = Profiles
		
		if LeadingEdgeCurve != None:
			self.leadingEdge = LeadingEdgeCurve
		
		if TrailingEdgeCurve != None:
			self.trailingEdge = TrailingEdgeCurve
		
		if RakeOutlineCurve != None:
			self.rakeOutline = RakeOutlineCurve
		
		self.listerners = []
		self.profileEditor = None
		self.refreshSurfaces()
	
	def registerListener(self, listener):
		self.listerners.append(listener)
	
	def setSampling(self, nR, nC):
		print 'Sampling updated: ', self.nR, self.nC, '\t ->', nR, nC
		self.nR = nR
		self.nC = nC
		# todo: set all sampling parameters happy days
		self.pd.sampling = nR
		self.chord.sampling = nR
		self.skew.sampling = nR
		self.rake.sampling = nR
		
		self.leadingEdge.sampling = nR
		self.trailingEdge.sampling = nR
		self.rakeOutline.sampling = nR
		
		self.profiles.setSampling(self.nR, self.nC)
		
		self.basis.setSampling(self.nR, self.nC)
		self.pressureSide.setSampling(self.nR, self.nC)
		self.suctionSide.setSampling(self.nR, self.nC)
		self.LE.setSampling(self.nR, self.nT)
		self.TE.setSampling(self.nR, self.nT)
		self.tip.setSampling(self.nC, self.nT)
		
		self.refreshSurfaces(False)
		self.updateListeners(Propeller.EVT_Resample)
	
	def updateListeners(self, evt):
		print 'Trigger event: ', self.Events[evt]
		for l in self.listerners:
			l.refresh(evt)
	
	@staticmethod
	def initializeFromFile(cfg):
		config = ConfigParser.SafeConfigParser()
		fileObject = open(cfg)
		config.readfp(fileObject)
		diameter = config.getfloat('General', 'Diameter')
		nR = config.getint('General', 'RadiusSampling')
		nC = config.getint('General', 'ChordSampling')
		rpm = config.getfloat('General', 'Rpm')
		J = config.getfloat('General', 'J')
		pd = Curve.getCurvefromConfig(config, 'Pitch/Diameter_Curve', nR)
		chord = Curve.getCurvefromConfig(config, 'Chord_Curve', nR)
		skew = Curve.getCurvefromConfig(config, 'Skew_Curve', nR)
		rake = Curve.getCurvefromConfig(config, 'Rake_Curve', nR)
		profiles = ProfilesDB.getDBfromConfig(config, nR, nC)
		leadingEdge = Curve.getCurvefromConfig(config, 'LeadingEdgeOutline', nR)
		trailingEdge = Curve.getCurvefromConfig(config, 'TrailingEdgeOutline', nR)
		rakeOutline = Curve.getCurvefromConfig(config, 'RakeOutline', nR)
		prop = Propeller(diameter, nR, nC, pd, chord, skew, rake, profiles, leadingEdge, trailingEdge, rakeOutline)
		prop.rpm = rpm
		prop.J = J
		return prop
	
	def saveToFile(self, path):
		config = ConfigParser.ConfigParser()
		config.add_section('General')
		config.set('General', 'Diameter', self.diameter)
		config.set('General', 'Rpm', self.rpm)
		config.set('General', 'J', self.J)
		config.set('General', 'RadiusSampling', self.nR)
		config.set('General', 'ChordSampling', self.nC)
		self.pd.save(config, 'Pitch/Diameter_Curve')
		self.chord.save(config, 'Chord_Curve')
		self.skew.save(config, 'Skew_Curve')
		self.rake.save(config, 'Rake_Curve')
		self.profiles.save(config)
		self.leadingEdge.save(config, 'LeadingEdgeOutline')
		self.trailingEdge.save(config, 'TrailingEdgeOutline')
		self.rakeOutline.save(config, 'RakeOutline')
		config.write(open(path, 'w'))
	
	def refreshSurfaces(self, trigger=True):
		# reset the surfaces of our propeller definition
		print 'Updating surfaces definition'
		self.basis.reset()
		self.pressureSide.reset()
		self.suctionSide.reset()
		self.LE.reset()
		self.TE.reset()
		self.tip.reset()
		self.profiles.resample()
		
		# get the minimum radius defined
		rmin = min(min(self.pd.x), min(self.chord.x), min(self.skew.x), min(self.rake.x))
		
		# radii list, based on the number of samples
		radii = np.linspace(rmin, 1, self.nR)
		for i, r in enumerate(radii):
			
			# get the local chord, pitch, skew and rake for the local radii
			c = self.chord.get(r)
			p = self.pd.get(r)*2
			s = self.skew.get(r)
			ra = self.rake.get(r)
			
			# calculate the local angles for the current radii
			phi = np.arctan(p/(2*np.pi*r))
			dTheta = c * np.cos(phi) / r
			for j, xc in enumerate(np.linspace(0, 1, self.nC)):
				# get the local offsset and thickness
				localOffset, localThickness = self.profiles.getData(i, j)
				localOffset *= c
				localThickness *= c
				dxPs = localOffset*np.sin(np.pi/2.0 - phi)
				dyPs = localOffset*np.cos(np.pi/2.0 - phi)
				dxSs = dxPs + localThickness*np.sin(np.pi/2.0 - phi)
				dySs = dyPs + localThickness*np.cos(np.pi/2.0 - phi)
				nu = dyPs / r
				mu = dySs / r
				psi = (xc-0.5)*dTheta
				self.basis.set(i, j, (xc-0.5)*c*np.sin(phi)+ra, r*np.cos(np.radians(s)+psi), r*np.sin(np.radians(s)+psi))
				self.pressureSide.set(i, j, (xc-0.5)*c*np.sin(phi)-dxPs+ra, r*np.cos(np.radians(s)+nu+psi), r*np.sin(np.radians(s)+nu+psi))
				self.suctionSide.set(i, j, (xc-0.5)*c*np.sin(phi)-dxSs+ra, r*np.cos(np.radians(s)+mu+psi), r*np.sin(np.radians(s)+mu+psi))
				if i== self.nR-1 or j == 0 or j == self.nC-1:
					for k, t in enumerate(np.linspace(0, 1, self.nT)):
						dxt = dxPs + t*localThickness*np.sin(np.pi/2.0 - phi)
						dyt = dyPs + t*localThickness*np.cos(np.pi/2.0 - phi)
						mut = dyt / r
						if j == 0:
							surf = self.LE
						else:
							surf = self.TE
						surf.set(i, k, (xc-0.5)*c*np.sin(phi)-dxt+ra, r*np.cos(np.radians(s)+mut+psi), r*np.sin(np.radians(s)+mut+psi))
						
						if i== self.nR-1:
							if j == 0 or j == self.nC-1:
								p = surf.getPoint(i, k)
							else:
								p = [(xc-0.5)*c*np.sin(phi)-dxt+ra, r*np.cos(np.radians(s)+mut+psi), r*np.sin(np.radians(s)+mut+psi)]
							self.tip.set(j, k, p[0], p[1], p[2])
		
		if trigger:
			self.updateListeners(Propeller.EVT_DiscretisationRefreshed)
	
	def updateCurvesFromShapeLines(self):
		print 'Updating curves'
		# building list of radii
		rmin = min(min(self.pd.x), min(self.chord.x), min(self.skew.x), min(self.rake.x))
		radii = np.linspace(rmin, 1, self.nR)
		
		newSkew = []
		newChord = []
		
		# set chord and skew value
		for r in radii:
			dTheta = self.trailingEdge.get(r) - self.leadingEdge.get(r)
			s = self.leadingEdge.get(r) + dTheta / 2.0
			dTheta = np.radians(dTheta)
			
			# get the local pitch
			p = self.pd.get(r)*2
			
			# calculate the local angles for the current radii
			phi = np.arctan(p/(2*np.pi*r))
			# calculate new chord
			c = (r * dTheta) / np.cos(phi)
			# self.chord.set(r, c)
			newChord.append(c)
			# self.skew.set(r, s)
			newSkew.append(s)
		
		self.chord = Curve(self.nR, radii, newChord)
		self.skew = Curve(self.nR, radii, newSkew)
		
		# calculate x coordinate of trailing edge point of first radius
		c0 = self.chord.get(0.2)
		p0 = self.pd.get(0.2)*2
		phi0 = np.arctan(p0/(2*np.pi*0.2))
		x0 = 0.5*c0*np.sin(phi0)
		
		newRake = []
		# set rake value
		for r in radii:
			c= self.chord.get(r)
			p = self.pd.get(r)*2
			phi = np.arctan(p/(2*np.pi*r))
			x = 0.5*c*np.sin(phi)
			newRake.append((x0 - x)+self.rakeOutline.get(r))
			#self.rake.set(r, (x0 - x)+self.rakeOutline.get(r))
		self.rake = Curve(self.nR, radii, newRake)
		
		self.updateListeners(Propeller.EVT_CurvesChanged)
		
		self.refreshSurfaces()
	
	def getAngleOfAttackDistribution(self):
		r, pd = self.pd.resample()
		return r, np.degrees(np.arctan(pd/(np.pi*r)) - np.arctan(self.J/(np.pi*r)))
	
	def adaptProfiles(self, index, correctAngle):
		r, theta = self.getAngleOfAttackDistribution()
		thetar = np.radians(theta)
		if index == 1:
			fctn = Profile.johnson3t
			fctnPrime = Profile.johnson3tPrime
			profile = Profile.J3t
		else:
			fctn = Profile.tulin
			fctnPrime = Profile.tulinPrime
			profile = Profile.Tulin
		
		for i, aoa in enumerate(thetar):
			
			if index == 1:
				x = (23.0/144.0+np.sqrt(7)/18.0)
			else:
				x = (1.0/16.0)
			k1 = fctnPrime(x)
			
			if correctAngle:
				k2 = fctn(1)
				A = Propeller.getA(k1, k2, aoa)
			else:
				A = np.tan(aoa)/k1
			
			self.profiles.set(r[i], profile(A, correctAngle, self.nC))
		
		self.updateListeners(Propeller.EVT_ProfilesDBModified)
		
		self.refreshSurfaces()
	
	def export(self, filename, separate, fileFormat):
		radius = self.diameter*0.5
		if not separate:
			self.pressureSide.export('PressureSide', radius, filename, fileFormat, 'w')
			self.suctionSide.export('SuctionSide', radius, filename, fileFormat, 'a')
			self.LE.export('LeadingEdge', radius, filename, fileFormat, 'a')
			self.TE.export('TrailingEdge', radius, filename, fileFormat, 'a')
			self.tip.export('Tip', radius, filename, fileFormat, 'a')
		else:
			fname = filename.split('_.')
			self.pressureSide.export('PressureSide', radius, fname[0]+'_PressureSide.'+fname[1], fileFormat, 'w')
			self.suctionSide.export('SuctionSide', radius, fname[0]+'_SuctionSide.'+fname[1], fileFormat, 'w')
			self.LE.export('LeadingEdge', radius, fname[0]+'_LeadingEdge.'+fname[1], fileFormat, 'w')
			self.TE.export('TrailingEdge', radius, fname[0]+'_TrailingEdge.'+fname[1], fileFormat, 'w')
			self.tip.export('Tip', radius, fname[0]+'_Tip.'+fname[1], fileFormat, 'w')
	
	@staticmethod
	def getA(k1, k2, theta):
		ttheta = np.tan(theta)
		a = k1*k2*ttheta
		b = k2 - k1
		c = ttheta
		
		D = np.power(b,2) - (4*a*c)
		A1 = -(b + np.sqrt(D))/(2*a)
		A2 = -(b - np.sqrt(D))/(2*a)
		
		return min(np.abs(A1), np.abs(A2))
